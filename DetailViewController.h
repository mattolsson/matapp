//
//  DetailViewController.h
//  MatApp
//
//  Created by MattiasO on 2015-03-12.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTableViewController.h"

@interface DetailViewController : UIViewController

@property (nonatomic) NSNumber *foodId;

@end
