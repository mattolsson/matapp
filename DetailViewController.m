//
//  DetailViewController.m
//  MatApp
//
//  Created by MattiasO on 2015-03-12.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (nonatomic) NSDictionary *dict;
@property (nonatomic) NSDictionary *food;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *kjLabel;
@property (weak, nonatomic) IBOutlet UILabel *kcalLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbsLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIImageView *nyckel;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    [self loadData];
}

- (void) loadData {
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.foodId];
    NSURL *url = [NSURL URLWithString:searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   NSLog(@"Completed! Data: %@, Error: %@", data, error);
                   NSError *parsingError = nil;
                   self.dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                   
                   if(!parsingError) {
                       
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           if(self.dict > 0) {
                               
                               self.food = self.dict[@"nutrientValues"];
                               
                               self.label.text = self.dict[@"name"];
                               self.kjLabel.text = [NSString stringWithFormat:@"%@ kJ", self.food[@"energyKj"]];
                               self.kcalLabel.text = [NSString stringWithFormat:@"%@ kcal", self.food[@"energyKcal"]];
                               self.proteinLabel.text = [NSString stringWithFormat:@"%@ g", self.food[@"protein"]];
                               self.carbsLabel.text = [NSString stringWithFormat:@"%@ g", self.food[@"carbohydrates"]];
                               self.fatLabel.text = [NSString stringWithFormat:@"%@ g", self.food[@"fat"]];
                               
                               NSNumber *fatNum = self.food[@"fat"];
                               NSNumber *kcalNum = self.food[@"energyKcal"];
                               if([fatNum intValue] <= 20 && [kcalNum intValue] <= 200) {
                                   [self setDynamics];
                               }
                           }else {
                               NSLog(@"No topics found");
                           }
                       });
                   }else {
                       NSLog(@"ParsingError is: %@", parsingError);
                   }
                   
               }];
    [task resume];  
}

- (void)setDynamics {
    
    self.nyckel = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
    
    self.nyckel.center = CGPointMake(self.view.center.x, 0);
    self.nyckel.image = [UIImage imageNamed:@"nyckelhal"];
    [self.view addSubview:self.nyckel];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.nyckel]];
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.nyckel]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];

}

@end
