//
//  ViewController.m
//  MatApp
//
//  Created by MattiasO on 2015-03-03.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fadeIn];
}

- (void)fadeIn {
    [self.label setAlpha:0.0f];
    
    [UIView animateWithDuration:3.0f animations:^{
        [self.label setAlpha:1.0f];
    }];
    
    [self.searchButton setAlpha:0.0f];
    
    [UIView animateWithDuration:3.0f animations:^{
        [self.searchButton setAlpha:1.0f];
    }];
}


@end
