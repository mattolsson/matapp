//
//  SearchTableViewController.h
//  MatApp
//
//  Created by MattiasO on 2015-03-03.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "TableViewCell.h"

@interface SearchTableViewController : UITableViewController <UISearchBarDelegate>

@end
