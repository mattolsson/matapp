//
//  SearchTableViewController.m
//  MatApp
//
//  Created by MattiasO on 2015-03-03.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "SearchTableViewController.h"
#import "DetailViewController.h"
#import "TableViewCell.h"

@interface SearchTableViewController ()

@property (nonatomic) NSArray *foodArray;
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) NSArray *root;
@property (nonatomic) NSNumber *foodId;


@end

@implementation SearchTableViewController



- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self loadData];
}

- (void)loadData {
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query="];
    NSURL *url = [NSURL URLWithString:searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   NSError *parsingError = nil;
                   self.root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                   
                   if(!parsingError) {
                       
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                       
                           if(self.root > 0) {
                               self.foodArray = self.root;
                               [self.tableView reloadData];
                           }else {
                               NSLog(@"No topics found");
                           }
                       });
                   }else {
                       NSLog(@"ParsingError is: %@", parsingError);
                   }
                   
               }];
    [task resume];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.tableView) {
        return self.foodArray.count;
    } else {
        return self.searchResult.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    if(tableView == self.tableView) {
        cell.textLabel.text = self.foodArray[indexPath.row][@"name"];
        cell.cellId = self.foodArray[indexPath.row][@"number"];
    } else {
        cell.textLabel.text = self.searchResult[indexPath.row][@"name"];
        cell.cellId = self.searchResult[indexPath.row][@"number"];
    }
    
    
    return cell;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    self.searchResult = [self.foodArray filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showDetailView"]) {
        TableViewCell *cellNumber = sender;
        DetailViewController *detail = [segue destinationViewController];
        detail.foodId = cellNumber.cellId;
    }
}


@end
