//
//  TableViewCell.h
//  MatApp
//
//  Created by MattiasO on 2015-03-16.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (nonatomic) NSNumber *cellId;

@end
